import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MultiLevelBarModule } from './multi-level-bar/multi-level-bar.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MultiLevelBarModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

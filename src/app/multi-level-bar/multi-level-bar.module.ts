import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CapacityBarComponent } from './components/capacity-bar/capacity-bar.component';
import { MultiLevelBarComponent } from './multi-level-bar.component';
import { BytesFormatPipe } from './pipes/bytes-format.pipe';

@NgModule({
  declarations: [
    MultiLevelBarComponent,
    CapacityBarComponent,
    BytesFormatPipe
  ],
  imports: [
    BrowserModule
  ],
  exports: [MultiLevelBarComponent]
})
export class MultiLevelBarModule { }

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BehaviorSubject, combineLatest, map, Observable } from 'rxjs';

@Component({
  selector: 'app-multi-level-bar',
  templateUrl: './multi-level-bar.component.html',
  styleUrls: ['./multi-level-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiLevelBarComponent {

    totalCapacity$: BehaviorSubject<number>
    allocatedCapacity$: BehaviorSubject<number>
    usedCapacity$: BehaviorSubject<number>
    error$: Observable<string | null>

    @Input() set totalCapacity(value: number) {
      this.totalCapacity$.next(value)
    }
    @Input() set allocatedCapacity(value: number) {
      this.allocatedCapacity$.next(value)
    }
    @Input() set usedCapacity(value: number) {
      this.usedCapacity$.next(value)
    }

    allocatedPercent: number = 0
    usedPercent: number = 0

    constructor() {
      this.totalCapacity$ = new BehaviorSubject<number>(0)
      this.allocatedCapacity$ = new BehaviorSubject<number>(0)
      this.usedCapacity$ = new BehaviorSubject<number>(0)
      this.error$ = this.initErrorStream()
    }

    initErrorStream(): Observable<string | null> {
      return combineLatest([
        this.totalCapacity$,
        this.allocatedCapacity$,
        this.usedCapacity$
      ]).pipe(
        map(([total, allocated, used]) => {
          if (this.validateValues(total, allocated, used)) {
            this.updatePerecents(total, allocated, used)
            return null
          } else {
            return 'Invalid value'
          }
        })
      )
    }

    updatePerecents(total: number, allocated: number, used: number): void {
      this.allocatedPercent = allocated / total * 100
      this.usedPercent = used / allocated * 100
    }

    validateValues(total: number, allocated: number, used: number): boolean {
      return  (typeof total === "number") &&
              (typeof allocated === "number") &&
              (typeof used === "number") &&
              (allocated <= total && used <= allocated)
    }
}

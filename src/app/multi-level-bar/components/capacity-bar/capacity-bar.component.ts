import { AfterViewChecked, ChangeDetectionStrategy, Component, ElementRef, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-capacity-bar',
  templateUrl: './capacity-bar.component.html',
  styleUrls: ['./capacity-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CapacityBarComponent implements AfterViewChecked {

    @Input() capacityValue?: number
    @Input() percentValue: number = 100
    @Input() capacityType: 'total' | 'allocated' | 'used' = 'total'

    @ViewChild('percentBlock') percentBlock?: ElementRef;
    @ViewChild('valueBlock') valueBlock?: ElementRef;

    readonly minWidhForBorderRadius = 12
    readonly minWidhForVisibleValue = 55
    
    ngAfterViewChecked(): void {
        if (this.percentBlock) {
            if (this.percentBlock.nativeElement.offsetWidth <= this.minWidhForBorderRadius) {
                this.percentBlock.nativeElement.style.borderRadius = '12px 0 0 12px'
            } else {
                this.percentBlock.nativeElement.style.borderRadius = '12px'
            }
        }
        if (this.valueBlock) {
            if (this.valueBlock.nativeElement.offsetWidth < this.minWidhForVisibleValue) {
                this.valueBlock.nativeElement.style.visibility = 'hidden'
            } else {
                this.valueBlock.nativeElement.style.visibility = 'visible'
            }
        }
    }

}

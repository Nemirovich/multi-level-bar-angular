import { Pipe, PipeTransform } from '@angular/core';
  
@Pipe({
    name: 'bytesFormat'
})
export class BytesFormatPipe implements PipeTransform {
  transform(value: number | null): string {
    if (typeof value !== "number") return '0 B'
    const k = 1024;
    const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(value) / Math.log(k));  
    const result = value / Math.pow(k, i)
    const dm = result < 10 ? 2 : result < 100 ? 1 : 0
    return parseFloat(result.toFixed(dm)) + ' ' + sizes[i];
  }
}
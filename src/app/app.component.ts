import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'multi-level-bar-angular';
  width = 500
  total: number = 100
  allocated: number = 50
  used: number = 1
}
